package com.c0d3in3.lecture18.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*


object ApiHandler{

    const val USERS_PAGE = "users?page="

    private var retrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://reqres.in/api/")
        .build()

    private var service: Requests = retrofit.create(Requests::class.java)

    interface Requests{
        @GET
        fun getRequest(@Url path: String?): Call<String?>
        @POST("{path}")
        fun postRequest(@Query("path") path : String) : Call<String?>
    }

    fun getRequestResponse(path: String?, callback: ApiInterface){
        service.getRequest(path).enqueue(object :Callback<String?>{
            override fun onFailure(call: Call<String?>, t: Throwable) {
                callback.onFail(t)
            }

            override fun onResponse(call: Call<String?>, response: Response<String?>) {
                println(response)
                callback.onSuccess(response.body().toString())
            }
        })
    }

}