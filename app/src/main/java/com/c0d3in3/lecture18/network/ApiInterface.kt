package com.c0d3in3.lecture18.network

interface ApiInterface {
    fun onSuccess(response: String){}
    fun onFail(throwable: Throwable){}
}