package com.c0d3in3.lecture18.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.c0d3in3.lecture18.R
import com.c0d3in3.lecture18.adapter.UsersAdapter
import com.c0d3in3.lecture18.model.*
import com.c0d3in3.lecture18.network.ApiHandler.USERS_PAGE
import com.c0d3in3.lecture18.network.ApiHandler.getRequestResponse
import com.c0d3in3.lecture18.network.ApiInterface
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
class MainActivity : AppCompatActivity() {

    //SWIPE DOWN TO LOAD ANOTHER PAGE and vice versa
    //SWIPE DOWN TO LOAD ANOTHER PAGE and vice versa
    //SWIPE DOWN TO LOAD ANOTHER PAGE and vice versa
    private var currentPage = "1"
    private val pageData = PageData()
    private val dataList = arrayListOf<AllData>()
    private lateinit var adapter : UsersAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){

        usersRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = UsersAdapter(dataList, this)
        usersRecyclerView.adapter = adapter

            makeRequest("$USERS_PAGE$currentPage")

        swipeLayout.setOnRefreshListener {
            swipeLayout.isRefreshing = true
            dataList.clear()
            adapter.notifyDataSetChanged()
            if(currentPage == "1") currentPage = "2"
            else currentPage = "1"
            makeRequest("$USERS_PAGE$currentPage")
        }

    }

    private fun makeRequest(path: String) {
        getRequestResponse(path, object : ApiInterface{
            override fun onSuccess(response: String) {
                parsePage(JSONObject(response))
            }

            override fun onFail(throwable: Throwable) {
                Toast.makeText(applicationContext, "There was failure with loading page", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun parsePage(json : JSONObject) {
        if(!json.isNull("page")) pageData.page = json.getInt("page")
        if(!json.isNull("per_page")) pageData.perPage = json.getInt("per_page")
        if(!json.isNull("total")) pageData.total = json.getInt("total")
        if(!json.isNull("total_pages")) pageData.totalPages = json.getInt("total_pages")
        if(!json.isNull("data")){
            val data = json.getJSONArray("data")
            for(i in 0 until data.length()){
                val jObject = data.getJSONObject(i)
                val userData = UserData()
                if(!jObject.isNull("id")) userData.id = jObject.getInt("id")
                if(!jObject.isNull("email")) userData.email = jObject.getString("email")
                if(!jObject.isNull("first_name")) userData.firstName = jObject.getString("first_name")
                if(!jObject.isNull("last_name")) userData.lastName = jObject.getString("last_name")
                if(!jObject.isNull("avatar")) userData.avatar = jObject.getString("avatar")
                dataList.add(userData)
            }
        }
        if(json.has("ad")){
            val ad = json.getJSONObject("ad")
            val adData = AdData()
            if(!ad.isNull("company")) adData.company = ad.getString("company")
            if(!ad.isNull("url")) adData.url = ad.getString("url")
            if(!ad.isNull("text")) adData.text = ad.getString("text")
            dataList.add(adData)
        }
        adapter.notifyDataSetChanged()
        swipeLayout.isRefreshing = false
        labelTextView.text = "Page ${pageData.page}"
    }


}

